BEAM README file.
Copyright (C) 2012-2014 Sergey Poznyakoff
See the end of file for copying conditions.

* Introduction

BEAM is a flexible and configurable tool for backup management.  Its
name is an acronym for "Backup Easy And Manageable".  The tool
provides utilities for backing up files and databases, restoring them
from existing backups and examining backup archives.

BEAM is written in a reasonably compatible POSIX shell language.  The
only deviation from the POSIX standard is that it makes use of the
"local" keyword.

* Prerequisites

1. A POSIX-compatible shell that supports the "local" keyword.  Most
existing shells do.

2. GNU tar version 1.16 or newer.

3. For backing up MySQL databases: mysql and mysqldump.

4. For backing up PostgreSQL databases: PostgreSQL.  In fact, the
following utilities are used: pg_dump, pg_restore, dropdb, createdb,
and psql.

5. For backing up and restoring LDAP databases: slapcat and slapadd.

6. If you plan to keep backups on Amazon S3 storage: s3backer.  It is
available from http://code.google.com/p/s3backer.

* Installation

The usual way:

 ./configure
 make
 # [become root]
 make install

The configure script provides a lot of options to tailor the
installation to your needs.

The most important options are:

  --prefix=DIR
     Use DIR as the installation prefix.  Installation prefix is
     appended to the normal paths when installing the system.  BEAM
     uses the following locations:

       PREFIX/etc          -- configuration directory
       PREFIX/libexec      -- directory for low-level beam utilities
       PREFIX/lib/beam     -- directory for beam modules.

  --sysconfdir=DIR
     Use DIR as the system configuration directory.

  --libdir=DIR
     Install BEAM modules in DIR/beam.

  --libexecdir=DIR
     Install low-level utilities in DIR.

  --with-s3
     Install modules for handling file systems backed by the Amazon
     Simple Storage Service (S3).

For the list of available generic options, consult the file INSTALL.
		    
* Overview

The "beam" utility works as a primary dispatcher tool.  It takes a
command and its options as the arguments.  The following commands
are available:

  beam backup         - runs a backup
  beam restore        - restores from a backup
  beam list           - lists what is included in a backup
  beam s3             - manages a s3-backed file system
  beam help COMMAND   - displays a short help for COMMAND

Configuration settings are read from the file beam.conf, located in the
system configuration directory (see above).

The package uses modular structure which makes it extremely flexible.
The low-level commands are located in the LIBEXECDIR.  For example,
running "beam backup" indirectly invokes LIBEXECDIR/beam-backup utility.

Backup methods are provided by modules, located in the directory
PREFIX/lib/beam.  Each module is a shell program providing primitives
for a particular task.  For example, the mysql.sh module handles
backups of MySQL databases.

* Documentation

The documentation in man page format is available:

  beam(1)
  beam-backup(1)
  beam-restore(1)
  beam-list(1)
  beam-s3(1)
  beam-module(5)
  beam.conf(5)

* Bug reporting

Send bug reports to <gray@gnu.org.ua>.


* Copyright information:

Copyright (C) 2012-2014 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim
   copies of this document as received, in any medium, provided that
   the copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:

