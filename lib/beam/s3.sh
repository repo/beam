#! /bin/sh
# This file is part of BEAM
# Copyright (C) 2012-2014 Sergey Poznyakoff
#
# BEAM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# BEAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BEAM.  If not, see <http://www.gnu.org/licenses/>.

# The configuration variable "backup_bucket_name" must contain the name
# of the s3 bucket to use.

# Raw bucket is mounted to /mnt/s3backer
test -z "$backup_mp_s3backer" && backup_mp_s3backer=/mnt/s3backer
# The actual file system is mounted to /mnt/s3
test -z "$backup_mp" && backup_mp_s3=/mnt/s3

s3_getmpoint()
{
    case $1 in
# Sample mount output, split into several lines:
# a) With --vhost:
# http://BUCKETNAME.s3.amazonaws.com/ on /mnt/s3backer type fuse.s3backer
#  (rw,nosuid,nodev,allow_other,default_permissions)
# b) Without --vhost:
# http://s3.amazonaws.com/BUCKETNAME/ on /mnt/s3backer type fuse.s3backer 
#  (rw,nosuid,nodev,allow_other,default_permissions)
	backer)
	    mount -tfuse.s3backer | 
            awk '/https?:\/\/.*'$backup_bucket_name'/ { print $3 }';;
	s3)
	    s=$(mount | grep "^${backup_mp_s3backer}/file" | awk '{ print $3 }')
	    if [ -z "$s" ]; then
                # Some kernels print device name instead of the actual mount file:
		s=$(mount | grep "^/dev/loop[0-9] on ${backup_mp_s3}" |
		    awk '{ print $3 }')
	    fi
	    echo $s;;
	*)
	    abent 1 "invalid usage of getmpoint"
    esac
}

# This variable is populated by s3_mount and is used by s3_unmount to unmount
# s3-backed file system.
umount_list=""

s3_mount() {
    # Make sure both mountpoints exist
    test -d $backup_mp_s3backer || mkdir $backup_mp_s3backer
    test -d $backup_mp_s3 || mkdir $backup_mp_s3

    set -- $(s3_getmpoint backer)
    if test -z "$1"; then
	$dry_run s3backer $backup_s3backer_options \
                      $backup_bucket_name $backup_mp_s3backer ||
	     abend 1 "unable to mount $backup_bucket_name"
	     umount_list="$backup_mp_s3backer"
    else
	backup_mp_s3backer=$1
    fi
    set -- $(s3_getmpoint s3)
    if test -z "$1"; then
	case $(basename $0) in
	    beam-restore|restore) mountopt=",ro";;
	    beam-backup|backup)   mountopt=",rw";;
	    beam-s3)  ;;
	    *)       error "called as $0: assuming default mount options"
	esac
	test -n "$backup_s3_mount_options" &&
	    mountopt="$mountopt,$backup_s3_mount_options"
        # NOTE: For ext4 add the journal_async_commit option.
	    $dry_run mount -oloop$mountopt \
		$backup_mp_s3backer/file $backup_mp_s3 || 
                   abend 1 "unable to mount $backup_mp_s3backer/file"
	      umount_list="$backup_mp_s3 $umount_list"
    else
	backup_mp_s3=$1
    fi
    epilogue_hook="s3_unmount $epilogue_hook"
}

s3_unmount()
{
    if test "$1" = "--force"; then
	for id in s3 backer
	do
	    mpoint=$(s3_getmpoint $id)
	    test -n "$mpoint" && $dry_run umount $mpoint
	done
    else
	for mpoint in $umount_list
	do
	    test -n "$mpoint" && $dry_run umount $mpoint
	done
    fi  
}
