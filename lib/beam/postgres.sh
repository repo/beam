#! /bin/sh
# This file is part of BEAM
# Copyright (C) 2012-2014 Sergey Poznyakoff
#
# BEAM is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# BEAM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BEAM.  If not, see <http://www.gnu.org/licenses/>.

# Configuration keywords:
#
# item_type=postgres            [mandatory]
# item_database=STRING          [mandatory]

# postgres_check item
postgres_check() {
    local database
  
    eval database=\$${1}_database
    test -z "$database" && error "${1}_database not set" && return 1
    return 0
}

# postgres_list item prefix
postgres_list() {
    local database
  
    eval database=\$${1}_database
    echo "${2}PostgreSQL database $database"
}

# postgres_backup item
postgres_backup() {
    local database

    eval database=\$${1}_database
    logit "backing up PostgreSQL database $database"
    test -z "$database" && abend 1 "${1}_database not set"
    if [ -z "$dry_run" ]; then
	su postgres -c "pg_dump $verbose $database" > $backup_tmp_dir/$1-$week-$round-$level    
    else
	echo "su postgres -c \"pg_dump $verbose $database\" > $backup_tmp_dir/$1-$week-$round-$level"
    fi

    if [ $? -ne 0 ]; then
	tarerror=$((tarerror + 1))
	logit "failed"
    else
	logit "creating $1-$week-$round-$level.$tar_suffix"
	$dry_run tar $verbose $taroptions \
	      -f $backup_archive_dir/$1-$week-$round-$level.$tar_suffix \
              -C $backup_tmp_dir $1-$week-$round-$level
	tarcode $?
	$dry_run rm $backup_tmp_dir/$1-$week-$round-$level
    fi
}

postgres_restore() {
    local u database

    eval database=\$${1}_database
    logit "restoring PostgreSQL database $database"
    u=$(umask)
    trap "umask $u" 1 2 3 13 15
    umask 077
    $dry_run tar $verbose $taroptions \
           -f $backup_archive_dir/$1-$week-$round-$level.$tar_suffix
    e=$?
    tarcode $e
    if [ $e -eq 0 ]; then
	logit "restoring database from the dump"
	if [ -n "$dry_run" ]; then
	    cat <<-EOT
	su postgres -c "dropdb $database"
	su postgres -c "createdb $database"
	su postgres -c "psql -d $database -f $1-$week-$round-$level"
	rm $1-$week-$round-$level
EOT
	elif [ -r $1-$week-$round-$level ]; then
	    su postgres -c "dropdb $database"
	    su postgres -c "createdb $database"
	    su postgres -c "psql -d $database -f $1-$week-$round-$level" > db-$1.log
	    if grep ERROR db-$1.log >/dev/null; then
		error "errors occurred during restore; see db-$1.log for details"
		error "dump preserved in file $1-$week-$round-$level"
		tarerror=$((tarerror + 1))
	    else
		rm $1-$week-$round-$level
	    fi
	fi
    fi
    umask $u
    trap - 1 2 3 13 15
}
