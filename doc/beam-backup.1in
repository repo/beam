.\" This file is part of BEAM -*- nroff -*-
.\" Copyright (C) 2012-2014 Sergey Poznyakoff
.\"
.\" BEAM is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" BEAM is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with BEAM.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH BEAM\-BACKUP 1 "March 19, 2014" "BEAM" "BEAM User Reference"
.SH NAME
beam\-backup \- create a back up.
.SH SYNOPSIS
.B beam backup
[\fB\-vnN\fR] [\fB\-\-verbose\fR] [\fB\-l\fR \fIFILE\fR]\
 [\fB\-\-logfile\fR \fIFILE\fR]\
 [\fB\-\-dry\-run\fR]
       [\fB\-L\fR \fIN\fR] [\fB\-\-level\fR \fIN\fR]
       [\fB\-R\fR \fIN\fR] [\fB\-\-round\fR \fIN\fR] [\fB\-\-week\fR \fIN\fR]

.B beam backup
[\fB\-h\fR] [\fB\-\-help\fR]

.B beam backup
[\fB\-V\fR] [\fB\-\-version\fR]
.SH DESCRIPTION
The
.B beam\-backup
command creates full or incremental backups of file systems and other
objects described in its configuration file
.BR beam.conf .
The configuration file by default is searched in
.BR @SYSCONFDIR@ .
Another location can be specified by setting the environment variable
.B BACKUP_CONFIG
to the full pathname of the configuration file.
.PP
After loading the configuration file, the script examines its command
line for options that modify its behavior.  These are described in
detail in the section
.BR OPTIONS ,
below.
.PP
The script uses
.BR tar (1)
to produce incremental backups.  The produced archives are named using
the following pattern:
.sp
.nf
.in +2
.BR ITEM \- WEEK \- ROUND \- LEVEL . SUFFIX
.in
.fi
.sp
where:
.TP
.B ITEM
The name of the backup item.  Backup items are defined in the
configuration variable \fBbackup_items\fR and identify file systems
(or other objects, such as PostgreSQL databases) to backup.  See the
section
.B BACKUP ITEMS
in
.BR beam.conf (5),
for a detailed description.
.TP
.B WEEK
The number of week when this backup was taken.  This is a number of
week in year, formatted as a two-digit decimal number in the range 01
to 53.  Weeks are numbered starting with the first Sunday as the first
day of week 01. For more details, see
.BR strftime (3)
conversion
.BR %U .
.TP
.B ROUND
The \fBround number\fR of this backup.  This number identifies a
particular series of incremental backups (starting at level 1), taken
with respect to the same full backup.

Consider for example the following backup schedule.  A full backup is
taken each Sunday.  Then, during the week, incremental backups are
taken daily: level one backup on Monday, level two backup on Tuesday,
then again level one on Wednesday, and so on.  In this scheme you get
three rounds of backups each week.  See the
.B EXAMPLES
section for an example of this schedule.
.TP
.B LEVEL
Incremental level number.
.TP
.B SUFFIX
The value of \fBbackup_suffix\fR configuration variable.  If not set,
.B tar
is assumed.
.PP
The archives are stored in the directory specified by the
.B backup_archive_dir
configuration variable.  This directory can be a local as well as a
remote one.  To use a remote location, set this variable to
.sp
.nf
.in +2
.BR HOST : DIR
.in
.fi
.sp
where
.B HOST
is the IP address or hostname of the remote machine, and
.B DIR
is the pathname on its file system.  See the \fBThe GNU Tar Manual\fR,
section 9.2 \fBRemote Tape Server\fR for more information on the subject.
.PP
The corresponding snapshots are stored in the directory specified by
the \fBbackup_snapshot_dir\fR configuration variable and follow the
same scheme as the archives, except that
.B db
is used as their suffix.
.SH OPTIONS
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Increase verbosity.  This overrides the \fBbackup_verbose\fR
configuration variable.
.TP
\fB\-l\fR, \fB\-\-logfile\fR \fIFILE\fB
Log to FILE.  This overrides the \fBbackup_logfile\fR configuration
variable.
.TP
\fB\-n\fR, \fB\-\-dry\-run
Do nothing, print what would have been done.  This option also implies
.BR \-\-no\-logfile .

Useful to debug your configuration.
.TP
\fB\-N\fR, \fB\-\-no\-logfile
Do not use \fBbackup_logfile\fR variable, log to standard error
instead.
.TP
\fB\-L\fR, \fB\-\-level \fIN\fR
Create incremental dump level \fIN\fR.
.TP
\fB-R\fR, \fB\-\-round \fIN\fR
Mark archives with incremental round number \fIN\fR.
.TP
\fB\-\-week \fIN\fR
Force using week number \fIN\fR.  By default, the current week number
(as returned by
.BR strftime (3)
conversion
.BR %U )
is used.
.TP
\fB\-h\fR, \fB\-\-help\fR
Display a short help summary.
.TP
\fB\-V\fR, \fB\-\-version\fR
Display program version.
.SH EXAMPLES
Usually \fBbackup\fR is started as a cron job.  The following
example shows a root crontab designed so as to create full (level 0)
backups on Sundays, level 1 backups on Mondays, Wednesdays and Fridays
and level 2 backups on Tuesdays, Thursdays and Saturdays.
.sp
.nf
.in +2
# Create one full backup weekly.
0 3 * * sun  beam backup \-\-round 0 \-\-level 0
# Create incremental backups daily.
0 3 * * mon  beam backup \-\-round 0 \-\-level 1
0 3 * * tue  beam backup \-\-round 0 \-\-level 2
0 3 * * wed  beam backup \-\-round 1 \-\-level 1
0 3 * * thu  beam backup \-\-round 1 \-\-level 2
0 3 * * fri  beam backup \-\-round 2 \-\-level 1
0 3 * * sat  beam backup \-\-round 2 \-\-level 2
.in
.fi
.sp
.SH FILES
.TP
.BR @SYSCONFDIR@/beam.conf
Default configuration file.
.TP
.BR @LIBDIR@/beam
Backup module directory.  Whenever a beam item of type \fBT\fR is
requested, \fBbeam\fR will attempt to load from this directory a
file named \fBT.sh\fR.  This file provides methods and definitions
necessary to handle that particular item type.
.TP
.BR @LIBDIR@/beam/common.sh
Settings and definitions shared between
.BR beam\-backup ,
.BR beam\-restore (1),
and
.BR beam\-list (1).
.SH "SEE ALSO"
.BR beam (1),
.BR beam\-restore (1),
.BR beam\-list (1),
.BR beam.conf (5).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <@PACKAGE_BUGREPORT@>.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

